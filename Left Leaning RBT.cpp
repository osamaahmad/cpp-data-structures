#include <iostream>
#include <sstream>
#include <random>
#include <iomanip>
#include <cassert>
#include <set>
#include <vector>
#include <queue>

// to use freopen.
#pragma warning(disable : 4996)

#define WINDOWS
#define RED true
#define BLACK false

template<typename K, typename T>
class RBT {

public:

    class Node {

    public:

        const K key;
        T value;

        Node *left;
        Node *right;

        bool color;

        Node(const K &key, const T &value, Node *left, Node *right, bool color) :
                key(key), value(value), left(left), right(right), color(color) {}

        Node(K key, T value) : Node(key, value, nullptr, nullptr, RED) {}

        Node(K key) : Node(key, (T) 0) {}
    };

    Node *root = nullptr;
    size_t size = 0;

    void _validate(Node *node, int count, std::set<int> &set) {
        if (node == nullptr) {
            set.insert(count);
            return;
        }
        count += node->color == BLACK;
        _validate(node->left, count, set);
        _validate(node->right, count, set);
    }

    bool validate() {
        std::set<int> set;
        _validate(this->root, 0, set);
        return set.size() == 1;
    }

    void printInit() {
#ifdef WINDOWS
        std::cout << "\033[107m"; // set background white
        std::cout << "\033[30m"; // set foreground black
#endif
    }

    void printNode(Node *node) {
#ifdef WINDOWS
        // foreground is black by default.

        if (node->color == RED) {
            std::cout << "\033[31m"; // set foreground red
        }
#endif

        std::cout << std::setw(3); // not necessary.
        std::cout << node->value << ' ';

#ifdef WINDOWS
        std::cout << "\033[30m"; // set foreground black
#endif
    }

    void _printNode(Node *node) {
        // prints R or B after each child.
        // used to build a trees using construct() function.

        printNode(node);
        if (node->left->color == RED) {
            std::cout << "R ";
        } else {
            std::cout << "B ";
        }
    }

    void _printChildren(Node *node) {
        if (node == nullptr) {
            return;
        }

        printNode(node);
        std::cout << ": ";

        if (node->left) {
            _printNode(node->left);
        }

        if (node->right) {
            _printNode(node->right);
        }

        std::cout << '\n';

        _printChildren(node->left);
        _printChildren(node->right);
    }

    void printChildren() {
        std::cout << "Children:\n";
        _printChildren(root);
        std::cout << "\n\n";
    }

    void printLevels() {
        printInit();

        std::queue<Node *> q;
        if (root != nullptr) {
            q.push(root);
        }

        std::cout << std::left;

        int level = 0;
        size_t size_sum = 0;
        while (!q.empty()) {
            size_t size = q.size();
            size_sum += size;
            std::cout << "Level " << std::setw(3) << level <<
                      ", Size: " << std::setw(3) << size << ": ";

            while (size--) {
                Node *x = q.front();
                printNode(x);
                q.pop();

                if (x->left) q.push(x->left);
                if (x->right) q.push(x->right);
            }

            std::cout << '\n';
            level++;
        }

        std::cout << "Total size: " << size_sum << ".\n\n";
        assert(validate());
    }

    Node *find(Node *node, const K &key) {
        if (node == nullptr) {
            return nullptr;
        }

        if (key < node->key) {
            if (node->left == nullptr) {
                return node;
            }
            return find(node->left, key);
        }

        if (key > node->key) {
            if (node->right == nullptr) {
                return node;
            }
            return find(node->right, key);
        }

        return node;
    }

    T *find(const K &key) {
        Node *result = find(root, key);
        if (result == nullptr || result->key != key) {
            return nullptr;
        }
        return &result->value;
    }

    bool isRed(Node *node) {
        return node != nullptr && node->color == RED;
    }

    Node *rotateLeft(Node *node) {
        Node *x = node;
        Node *y = node->right;

        // no need to change the parent
        // since this will be called from
        // a recursive function.

        x->right = y->left;
        y->left = x;

        // in case both were red.
        y->color = x->color;
        // the child should be red.
        x->color = RED;

        return y;
    }


    Node *rotateRight(Node *node) {
        Node *x = node;
        Node *y = node->left;

        // no need to change the parent
        // since this will be called from
        // a recursive function.

        x->left = y->right;
        y->right = x;

        // in case both were red.
        y->color = x->color;
        // the child should be red.
        x->color = RED;

        return y;
    }

    void flipColors(Node *node) {
        assert(!isRed(node));
        assert(isRed(node->left));
        assert(isRed(node->right));

        // left and right won't be null
        // since they're red.

        node->color = RED;
        node->left->color = BLACK;
        node->right->color = BLACK;
    }

    Node *fixColorViolations(Node *node) {
        if (isRed(node->right) && !isRed(node->left))
            node = rotateLeft(node);

        // may change the logic here.
        // sometimes, this is just an undoing of the previous condition.
        if (isRed(node->left) && isRed(node->left->left))
            node = rotateRight(node);

        if (isRed(node->left) && isRed(node->right))
            flipColors(node);

        return node;
    }

    Node *insert(Node *node, const K &key, const T &value) {
        if (node == nullptr) {
            size++;
            return new Node{key, value};
        }

        if (key < node->key) {
            node->left = insert(node->left, key, value);
        } else if (key > node->key) {
            node->right = insert(node->right, key, value);
        } else {
            node->value = value;
        }

        node = fixColorViolations(node);

        return node;
    }

    void insert(const K &key, const T &value) {
        root = insert(root, key, value);
        root->color = BLACK;
    }

    struct RemoveReturn {
        Node *node;
        bool color;
    };

    void stealChildren(Node *node, Node *toSteal) {
        node->left = toSteal->left;
        if (node != toSteal->right) {
            node->right = toSteal->right;
        }
        node->color = toSteal->color;
    }

    RemoveReturn fixDeleteViolationsLeft(Node *node) {
        bool color = RED;

        // if a the left is deleted, then
        // definitely the right is not null.

        if (isRed(node->right->left)) {
            // case 1
            // black sibling with red child.

            node->right = rotateRight(node->right);
            node->right->right->color = BLACK;

            node = rotateLeft(node);
            node->left->color = BLACK;
        } else {
            // case 2
            // black sibling with black child.

            node->right->color = RED;

            if (isRed(node))
                node->color = BLACK;
            else
                color = BLACK;

            // since we've set the right child
            // to red which is prohibited in a
            // left leaning Red-Black tree.
            node = fixColorViolations(node);
        }

        return {node, color};
    }

    RemoveReturn fixDeleteViolationsRight(Node *node) {
        bool color = RED;

        // if a the right is deleted, then
        // definitely the left is not null.

        if (isRed(node->left)) {
            // case 3
            // red sibling.

            // after this, will deal
            // with a black sibling.
            node = rotateRight(node);

            // node->right is what used to be node before.
            RemoveReturn temp = fixDeleteViolationsRight(node->right);
            node->right = temp.node;

            if (temp.color == RED) {
                node = fixColorViolations(node);
                return {node, color};
            }
        }

        if (isRed(node->left->left)) {
            // case 1
            // black sibling with red child.
            node = rotateRight(node);
            node->left->color = BLACK;
            node->right->color = BLACK;
        } else {
            // case 2
            // black sibling with black child.
            node->left->color = RED;

            if (isRed(node))
                node->color = BLACK;
            else
                color = BLACK;
        }

        return {node, color};
    }

    RemoveReturn _swapWithSuccessor(Node *node, Node *toDelete, Node *&ret) {
        if (node == nullptr) {
            return {nullptr, BLACK};
        }

        if (node->left == nullptr) {
            RemoveReturn result = {nullptr, node->color};
            stealChildren(node, toDelete);
            ret = node;
            return result;
        }

        RemoveReturn result = _swapWithSuccessor(node->left, toDelete, ret);

        node->left = result.node;

        if (result.color == BLACK) {

            if (ret == toDelete->right)
                result = fixDeleteViolationsRight(node);
            else
                result = fixDeleteViolationsLeft(node);
        } else {
            result.node = fixColorViolations(node);
        }

        return result;
    }

    RemoveReturn swapWithSuccessor(Node *node) {
        Node *ret = nullptr;
        RemoveReturn result = _swapWithSuccessor(node->right, node, ret);
        ret->right = result.node;
        result.node = ret;
        return result;
    }

    RemoveReturn remove(Node *node, const K &key) {
        if (node == nullptr) {
            // not found
            return {nullptr, RED};
        }

        RemoveReturn result = {nullptr, RED};

        if (key < node->key) {

            result = remove(node->left, key);
            node->left = result.node;

            if (result.color == BLACK)
                result = fixDeleteViolationsLeft(node);
            else
                result.node = fixColorViolations(node);
        } else if (key > node->key) {

            result = remove(node->right, key);
            node->right = result.node;

            if (result.color == BLACK)
                result = fixDeleteViolationsRight(node);
            else
                result.node = fixColorViolations(node);
        } else {
            if (node->left == nullptr) {
                result.node = node->right;
                result.color = node->color;
            } else if (node->right == nullptr) {

                // if the deleted is red, nobody cares.
                // if black, if the child is red, just
                // turn it to black. otherwise, there is a double black.

                if (node->color == BLACK && isRed(node->left)) {
                    node->left->color = BLACK;
                    result.color = RED;
                } else {
                    result.color = node->color;
                }

                result.node = node->left;
            } else {
                result = swapWithSuccessor(node);

                if (result.color == BLACK)
                    result = fixDeleteViolationsRight(result.node);
                else
                    result.node = fixColorViolations(result.node);
            }

            size--;
            delete node;
        }

        return result;
    }

    void remove(const K &key) {
        root = remove(root, key).node;
        if (root != nullptr) {
            root->color = BLACK;
        }
    }

    bool empty() {
        return size == 0;
    }
};

void construct(RBT<int, int> &rbt) {
    // bad ugly code, leaks memory, only for testing.

    int n = 10000;

    std::vector<RBT<int, int>::Node *> v(n);
    for (int i = 0; i < n; i++) {
        v[i] = new RBT<int, int>::Node{i, i};
    }

    int parent;
    std::string s;
    while (std::getline(std::cin, s)) {
        std::istringstream iss(s);

        if (!(iss >> parent)) break;

        char colon;
        iss >> colon;

        if (rbt.root == nullptr) {
            rbt.root = v[parent];
            rbt.root->color = BLACK;
        }

        int children[2];
        char color[2];
        int i = 0;
        while (iss >> children[i]) {
            iss >> color[i++];
        }

        if (!i) continue;

        if (i == 1) {

            if (children[0] < parent)
                v[parent]->left = v[children[0]];
            else
                v[parent]->right = v[children[0]];
        } else {
            v[parent]->left = v[children[0]];
            v[parent]->right = v[children[1]];
        }

        v[children[0]]->color = (color[0] == 'R' ? RED : BLACK);
        v[children[1]]->color = (color[1] == 'R' ? RED : BLACK);
    }
}

int randomNum(int min, int max) {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist(min, max);
    return dist(rng);
}

RBT<int, int> rbt;

void buildTree() {
    freopen("in.txt", "r", stdin);

    construct(rbt);
    rbt.printLevels();
    rbt.printChildren();

    int node;
    while (std::cin >> node) {
        rbt.remove(node);
        rbt.printLevels();
        rbt.printChildren();
    }
}

void insertRange(int min, int max, bool print, bool printLevels, bool printChildren) {
    if (print)
        std::cout << "Insert the range [" << min << ", " << max << "]\n\n";

    for (int i = min; i <= max; i++) {
        rbt.insert(i, i);
        if (print) {
            if (printLevels) rbt.printLevels();
            if (printChildren) rbt.printChildren();
        }
    }
}

void deleteSelected(const std::vector<int> &v) {
    std::cout << "Delete selected \n\n";
    for (auto i : v) {
        std::cout << "size: " << rbt.size << ", deleting " << i << "....\n";
        rbt.printLevels();
        rbt.remove(i);
    }
    rbt.printLevels();
}

void emptyRandomly(int min, int max, bool print, bool printLevels, bool printChildren) {
    if (print)
        std::cout << "empty randomly \n\n";

    std::vector<int> idxs;
    for (int i = min; i <= max; i++) {
        idxs.push_back(i);
    }

    while (!rbt.empty()) {
        int remaining = idxs.size();
        int idx = randomNum(0, remaining - 1);
        int num = idxs[idx];
        idxs.erase(idxs.begin() + idx);
        if (print) {
            std::cout << "size: " << rbt.size << ", remaining: " <<
                      remaining << ", idx: " << idx << ", deleting " << num << "....\n";
            if (printLevels) rbt.printLevels();
            if (printChildren) rbt.printChildren();
        }
        assert(rbt.size == remaining);
        rbt.remove(num);
    }

    if (print && printLevels)
        rbt.printLevels();
}

void deleteHalf(int n) {
    std::cout << "Delete 3 \n\n";

    for (int i = 1; i <= n / 2 + 1; i++) {
        rbt.remove(i);
        rbt.printLevels();
    }
}

void insertHalf(int n) {
    std::cout << "Insert 2 \n\n";

    for (int i = 1; i <= n / 2 + 1; i++) {
        rbt.insert(i, i);
        rbt.printLevels();
    }
}

void stressTest(int min, int max, int repeat) {
    for (int t = 0; t < repeat; t++) {
        insertRange(min, max, false, false, false);
        emptyRandomly(min, max, false, false, false);
    }
    std::cout << "Stress test done.\n\n";
}

int main() {
    stressTest(1, 1000, 1000);
}